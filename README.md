# Docker

Laravel + Vite project

-   Create an .env file

```http
cp .env.example .env
```

-   Fill in the fields to connect to the database

```http
DB_CONNECTION=mysql
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=ftmo_db
DB_USERNAME=root
DB_PASSWORD=root
```

-   For the first time only, run the command:

```http
make create-backend
```

-   Next time there is no need to download all the dependencies, so just run the command:

```http
make up-backend
```

-   After starting the containers required for the backend, install all the frontend's dependencies:

```http
make npm-install
```

-   And run the command to start the frontend:

```http
make up-frontend
```

-   To stop containers, you can use the following commands:

```http
make stop-backend
make stop-frontend
```
