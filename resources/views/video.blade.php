<x-app-layout>
	<x-container>

		<x-topbar class="mb-8">
			<x-slot:title>
				{{ __('Dashboard') }}
			</x-slot:title>
		</x-topbar>
        <div id="sh-list_video-9074"></div>
    </x-container>
</x-app-layout>

<script>
    $(function() {
        shMediaVideo({
            'key': '{{env('MEDIAHUB_KEY')}}',
            'cid': '29',
            'tags': '',
            'lang': 'en',
            'blockID': 'sh-list_video-9074'
        });
    });
</script>
