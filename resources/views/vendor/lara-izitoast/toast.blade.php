<script>
	@foreach (session('toasts', collect())->toArray() as $toast)
		var options = {
			title: '{{ $toast['title'] }}',
			message: '{{ $toast['message'] }}',
			messageColor: '{{ $toast['messageColor'] }}',
			position: '{{ $toast['position'] }}',
			titleColor: '{{ $toast['titleColor'] }}',
			closeOnClick: '{{ $toast['closeOnClick'] }}',
		};

		var type = '{{ $toast['type'] }}';

		show(type, options);
	@endforeach
	function show(type, options) {
		const iziToast = window.iziToast;
		if (iziToast) {
			console.log(options);
			if (type === 'info') {
				iziToast.info(options);
			} else if (type === 'success') {
				iziToast.success(options);
			} else if (type === 'warning') {
				iziToast.warning(options);
			} else if (type === 'error') {
				iziToast.error(options);
			} else {
				iziToast.show(options);
			}
		} else {
			setTimeout(() => show(type, options), 200);
		}
	}
</script>

{{ session()->forget('toasts') }}
