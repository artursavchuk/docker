<x-app-layout>
	<x-container>

		<x-topbar class="mb-8">
			<x-slot:title>
				{{ __('Dashboard') }}
			</x-slot:title>
		</x-topbar>

		Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit vel eveniet, maxime impedit cupiditate doloribus
		suscipit sint temporibus rerum, labore cumque autem alias. Laudantium non a aperiam officia eos modi minima optio,
		voluptate quibusdam iure dicta nulla aliquid aspernatur minus molestias, id quae perferendis repellat rerum, est
		doloremque fugiat praesentium. Dicta in ipsam delectus facilis corporis similique possimus perferendis saepe,
		consequatur quam aliquid. Ullam quibusdam porro incidunt libero dolorum odio voluptate ipsum quia aut quas sed
		perferendis nemo tempora magnam animi non ea accusantium a atque quidem, eveniet fuga et quo? Illo at eius ipsa cumque
		itaque architecto! Eaque, harum. Porro commodi rem aspernatur earum, illo corrupti totam ipsum, ducimus, eius
		doloribus culpa voluptate esse dolorum laudantium veniam laboriosam expedita aperiam numquam id iste impedit! Quam
		voluptatem natus iusto dolorum atque libero numquam, illum magnam architecto, sapiente omnis quidem ipsam voluptas eos
		nihil sed officiis aliquam! Aperiam tenetur similique sequi dolores doloribus totam, sunt nihil. Aspernatur fuga
		corrupti magni quae tempora quasi consequatur at. Odit laborum exercitationem quod rerum ducimus ipsa ullam, quas non
		ipsam magni ex placeat repellendus qui, ad laboriosam sequi quae in! Sint incidunt quas temporibus voluptates ratione,
		repellendus odio est numquam nulla sapiente sit natus obcaecati accusantium fugiat qui enim sequi. Voluptates
		reprehenderit nulla quos temporibus fuga maxime voluptate accusantium rerum cumque voluptatem quia consequuntur
		quaerat dignissimos, harum incidunt. In suscipit autem quae, ex animi necessitatibus magnam quisquam maiores quas
		alias amet officiis provident fugit harum ab nemo repudiandae explicabo libero iste expedita quo? Doloremque sunt nam
		laudantium maxime error aspernatur at rerum, voluptate incidunt consequuntur, ratione cumque culpa in atque
		architecto, magni fugiat facilis ipsam. Consequatur vitae neque ducimus repellat deserunt id non rem maxime nisi sint
		blanditiis repellendus nulla quia eum dolorem labore, numquam minima, qui voluptates? Nobis temporibus unde quia
		provident ab in ex quam impedit nostrum distinctio illo officiis, vero aliquid laborum beatae architecto incidunt?
		Animi aspernatur facere possimus ex dicta numquam vero, porro quisquam incidunt, sunt aliquid vitae necessitatibus
		dolorem optio nisi quam, ipsum nobis atque corporis perferendis cupiditate. Quas animi aliquam dolorum repellat optio
		impedit natus facilis, necessitatibus dicta magni at quaerat dolores nisi distinctio dolorem repellendus praesentium
		iusto sit eos ipsum voluptas nam quia? Hic ullam culpa nam commodi nostrum repellat amet assumenda quibusdam, nemo
		asperiores quisquam inventore vel sint harum vero perspiciatis ut dolorem alias modi doloremque non! Nam repellendus
		quidem sapiente sequi minus officia, odio ducimus ea itaque asperiores similique, placeat ipsam doloribus quae! Dicta
		iusto consectetur temporibus quas repellendus optio rem amet, beatae sint in eveniet ullam totam cumque? Nobis quidem
		labore laborum aspernatur odio sequi quos rerum atque itaque assumenda, recusandae fuga ratione consequatur corrupti
		et esse, aliquid, saepe quo excepturi sed? Ratione voluptatem numquam voluptatum necessitatibus corporis?
		Reprehenderit, velit omnis! Non est magnam natus animi fuga consectetur, sapiente excepturi ut quaerat cumque
		laudantium quasi eum facilis quos! Quos eius excepturi voluptatem. Maiores, culpa. Possimus, eos? Sit cumque nemo,
		aliquid ipsam beatae temporibus quisquam ut ipsa cupiditate nam a hic unde perspiciatis consequuntur vero quibusdam
		qui quia voluptatem explicabo eos similique dolorum. Earum rerum quaerat quas et minima obcaecati placeat totam hic
		pariatur soluta ab enim ullam repudiandae eveniet harum veritatis quibusdam incidunt ipsam expedita, id quos ducimus
		esse veniam illo? Illum laudantium quae consequuntur nam, pariatur nesciunt molestiae praesentium enim eos! Ipsum
		voluptates unde adipisci exercitationem, harum, qui omnis magni quod similique reiciendis dolorem? Laboriosam,
		incidunt dicta rerum libero optio harum quos explicabo voluptatibus eveniet quas veritatis quo facilis itaque?
		Pariatur mollitia, accusamus eos harum quia saepe, incidunt molestias nulla laboriosam dignissimos, quisquam tenetur
		repudiandae. Incidunt, corrupti iure, qui quae aperiam exercitationem temporibus possimus odio in explicabo pariatur
		fuga error nemo omnis nisi unde consectetur, velit ex eius molestiae eligendi. Repudiandae nulla quo est. Expedita
		ipsa architecto voluptate vero ut minima aliquid in odit molestiae dolor, tempore harum, voluptatibus, distinctio quas
		sit placeat temporibus. Vitae, incidunt ea. Exercitationem ratione unde, voluptatum accusantium perspiciatis officiis
		delectus dicta quis blanditiis impedit optio molestiae obcaecati quidem reiciendis sunt maxime in quae laboriosam
		porro sequi illo ullam rerum. Ducimus suscipit consectetur, explicabo illum odio nemo officiis sequi blanditiis
		laboriosam, dicta enim atque. Mollitia vel excepturi necessitatibus dolore ut unde consequatur cupiditate reiciendis
		porro blanditiis, quibusdam doloribus, molestiae autem? Aliquam eos molestiae minima nisi eligendi! Harum ipsum labore
		vel iure incidunt sequi voluptas illo obcaecati eos animi. Quo repellat officia est voluptate facere saepe? Amet culpa
		sit dolores beatae veritatis maxime consequuntur sed temporibus! Perspiciatis omnis dolorum mollitia porro! Amet,
		impedit possimus soluta ut, a consectetur officiis minima debitis ea facere aperiam sit esse adipisci eaque
		consequuntur. Eius necessitatibus modi, odit rerum laboriosam quo impedit officiis? Similique, recusandae minus
		mollitia incidunt aperiam ullam aliquid ducimus nulla, facere, maiores ad sit voluptas exercitationem sequi ab dolore
		quasi provident accusamus iusto assumenda quos corrupti explicabo. Veritatis omnis fugiat expedita fuga reiciendis.
		Fuga quibusdam nam maxime aliquam perspiciatis, eaque inventore ad praesentium omnis vel excepturi voluptas libero!
		Eaque voluptate obcaecati ipsam id, blanditiis quibusdam laboriosam voluptatem vero quaerat rem velit non dignissimos
		laborum libero dolor. Delectus, totam. Fuga, possimus excepturi sint ut similique quibusdam accusantium nostrum veniam
		iure qui aut odio explicabo animi eos. Repudiandae minima nulla, architecto accusamus, illum esse officiis labore
		quisquam sapiente, nemo quaerat quasi sit! Inventore a soluta at, veritatis illum voluptate obcaecati. Ut delectus
		eaque libero harum, optio nesciunt itaque perspiciatis voluptas fuga provident ducimus esse nisi dolorem facilis
		assumenda saepe ullam sit quia magni. Sequi, eum dolor itaque, vel eius fugit quod hic odio optio est quis accusamus
		deleniti expedita accusantium molestias doloremque possimus dolore similique illum deserunt distinctio. Sit ipsum, ut
		minus facere molestias magnam beatae mollitia iure voluptatem, officiis dolorem. Earum labore placeat repellendus
		similique ratione nihil harum laudantium consequatur! Esse eos aperiam amet reprehenderit nihil adipisci veniam
		quibusdam tempora ratione voluptates. Debitis excepturi deleniti repudiandae deserunt maxime vitae ea pariatur
		voluptatibus optio rem, cum nulla modi amet suscipit, quis totam eaque, autem eos. Exercitationem et dicta itaque
		sapiente delectus totam distinctio perspiciatis enim, accusantium dignissimos accusamus quos suscipit repudiandae eos
		illum laborum aliquid.

	</x-container>
</x-app-layout>
