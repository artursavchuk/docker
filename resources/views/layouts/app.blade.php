<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.js"
		integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU=" crossorigin="anonymous"></script>
	<link href="{{ env('MEDIAHUB_HOST') }}/theme/pub/view/css/shmedia.css" rel="stylesheet">
	<script src="{{ env('MEDIAHUB_HOST') }}/js/shmedia1.js?v=0.02"></script>

	@vite(['resources/css/app.scss', 'resources/js/app.js'])
</head>

<script>
	document.addEventListener('alpine:init', () => {
		Alpine.data('global', () => ({
			isOpenedMobileMenu: false,
		}))
	})
</script>

<body
	class="flex min-h-screen flex-col bg-dark bg-texture-1 pt-[60px] font-sans text-sm font-normal text-light antialiased"
	x-data="global" :class="{ 'overflow-y-hidden': isOpenedMobileMenu }">

	<x-circle class="opacity-30" id="circle1" />
	<x-circle class="opacity-30" id="circle2" />

	@include('layouts.header.header')

	<div class="grid grid-cols-1 sm:grid-cols-[250px_1fr]">
		@include('layouts.sidebar.sidebar')

		<main class="overflow-y-hidden py-12">
			{{ $slot }}
		</main>
	</div>

	{{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.3/cropper.css" rel="stylesheet"> --}}
</body>

</html>
