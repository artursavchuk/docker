<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>{{ config('app.name', 'Laravel') }}</title>

	@vite(['resources/css/app.scss', 'resources/js/app.js'])
</head>

<body class="h-[100dvh] overflow-hidden bg-dark bg-texture-1 font-sans text-sm font-normal text-light">
	@include('vendor.lara-izitoast.toast')
	<x-circle class="opacity-30" id="circle1" />
	<x-circle class="opacity-30" id="circle2" />

	<div class="col flex h-full">
		<div class="hidden sm:block sm:w-1/4 md:w-1/3 lg:w-1/2">
			<picture>
				<source srcset="/images/auth-bg/auth-bg.avif" type="image/avif">
				<source srcset="/images/auth-bg/auth-bg.webp" type="image/webp">
				<img class="h-full w-full object-cover" src="/images/auth-bg/auth-bg.jpg" alt="auth backbrount">
			</picture>
		</div>
		<div class="flex h-full flex-grow flex-col justify-center sm:w-3/4 md:w-2/3 lg:w-1/2" x-data="{

    currentLocale: '',
    setInitialLocale() {
        const currentUrl = new URL(window.location.href);
        this.currentLocale = currentUrl.searchParams.has('locale') ? currentUrl.searchParams.get('locale') : '{{ App::currentLocale() }}'
        currentUrl.searchParams.set('locale', this.currentLocale)
        window.history.replaceState({}, '', currentUrl.toString());
    },
    changeLocale(event) {
        const currentUrl = new URL(window.location.href);
        currentUrl.searchParams.set('locale', event.detail)
        window.history.replaceState({}, '', currentUrl.toString());
        window.location.reload()
    }
}"
			x-init="setInitialLocale">
			{{-- language switcher --}}
			<x-language-switcher class="r-0 sticky top-0 mx-6 my-3 ml-auto w-[max-content]" small ::currentLocale="currentLocale"
				x-on:change-locale="changeLocale" />

			{{-- Content --}}
			<div class="overflow-y-auto px-5 py-[5vh] sm:px-10">
				{{ $slot }}
			</div>
		</div>
	</div>
</body>

</html>
