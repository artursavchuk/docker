		<x-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
		{{ __('Dashboard') }}
		</x-link>

		<x-link :href="route('profile.index')" :active="request()->routeIs('profile.profile')">
		{{ __('Profile') }}
		</x-link>
