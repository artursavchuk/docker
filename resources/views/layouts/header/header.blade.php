@php
	$userName = ucfirst(Auth::user()->name);
	$maxLength = 15;
	if (strlen($userName) > $maxLength) {
	    $userName = substr($userName, 0, $maxLength) . '...';
	}
@endphp

<header class="fixed left-0 top-0 z-50 flex h-[60px] w-full items-center border-b border-primary-200 backdrop-blur-sm">
	<div class="flex w-full justify-between px-4 align-middle">
		{{-- Logo --}}
		<a class="flex items-center justify-center" href="{{ route('dashboard') }}">
			logo
		</a>

		{{-- settings --}}
		<div class="flex items-center gap-2">

			{{-- Dropdown --}}
			<div class="hidden sm:flex sm:items-center">
				<x-dropdown align="right" width="w-[max-content]">
					<x-slot name="trigger">
						<button
							class="inline-flex items-center px-3 py-2 text-gray-300 transition duration-150 ease-linear hover:text-white">
							<div class="flex items-center">
								@if (Auth::user()->avatar)
									<img class="mr-2 h-8 w-8 rounded-full border border-primary-200 object-cover"
										src="{{ asset(Auth::user()->avatar) }}" alt="avatar">
								@endif

								{{ $userName }}
							</div>

							<div class="ml-1">
								<svg class="h-4 w-4 fill-current" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20">
									<path fill-rule="evenodd"
										d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z"
										clip-rule="evenodd" />
								</svg>
							</div>
						</button>
					</x-slot>

					<x-slot name="content">
						<nav class="grid gap-4 px-4 py-4">
							<x-link class="w-full text-left" :href="route('profile.index')">
								{{ __('Profile') }}
							</x-link>

							<form method="POST" action="{{ route('logout') }}">
								@csrf

								<x-link class="w-full text-left" :href="route('logout')" component="button">
									{{ __('Log Out') }}
								</x-link>
							</form>
						</nav>
					</x-slot>
				</x-dropdown>
			</div>

			{{-- Hamburger --}}
			<button class="relative flex h-8 w-8 items-center justify-center focus:outline-none sm:hidden"
				x-on:click.away="isOpenedMobileMenu=false" x-on:click="isOpenedMobileMenu=!isOpenedMobileMenu">
				<span class="absolute block h-0.5 w-2/3 transform bg-white transition ease-linear"
					:class="{ 'rotate-45': isOpenedMobileMenu, ' -translate-y-1.5': !isOpenedMobileMenu }"></span>
				<span class="absolute block h-0.5 w-2/3 transform bg-white transition ease-linear"
					:class="{ 'opacity-0': isOpenedMobileMenu }"></span>
				<span class="absolute block h-0.5 w-2/3 transform bg-white transition ease-linear"
					:class="{ '-rotate-45': isOpenedMobileMenu, ' translate-y-1.5': !isOpenedMobileMenu }"></span>
			</button>
		</div>
	</div>

	{{-- Mobile Menu --}}
	<div
		class="l-0 fixed top-0 z-40 flex h-[100dvh] w-full -translate-x-full flex-col border-primary-200 bg-dark px-4 py-5 transition duration-200 min-[500px]:w-[300px] min-[500px]:border-r sm:hidden"
		:class="{ 'translate-x-0': isOpenedMobileMenu }" x-on:click.stop>

		{{-- Close button --}}
		<button class="absolute right-4 top-4 hidden h-8 w-8 items-center justify-center focus:outline-none max-[500px]:flex"
			x-on:click.stop="isOpenedMobileMenu=false">
			<span class="absolute block h-0.5 w-2/3 rotate-45 transform bg-white transition ease-linear"></span>
			<span class="absolute block h-0.5 w-2/3 -rotate-45 transform bg-white transition ease-linear"></span>
		</button>

		{{-- Profile --}}
		<div
			class="hover-icon hover:bg-secondary-100 dark:hover:bg-secondary-700 flex min-h-[72px] flex-row items-center gap-4 hover:bg-opacity-30 dark:hover:bg-opacity-30">
			@if (Auth::user()->avatar)
				<img class="mr-2 h-8 w-8 rounded-full border border-primary-200 object-cover"
					src="{{ asset(Auth::user()->avatar) }}" alt="avatar">
			@else
				<div
					class="bg-primary-600 flex h-10 w-10 items-center justify-center rounded-full bg-primary-200 text-base font-bold tracking-[0.15px] text-white">
					{{ strtoupper(substr(Auth::user()->name, 0, 1)) }}
				</div>
			@endif
			<div class="flex flex-grow flex-col">
				<p class="tracking-[.03125em]">{{ $userName }}</p>
				<span class="text-sm tracking-[0.25px]">{{ Auth::user()->email }}</span>
			</div>
		</div>

		{{-- Menu --}}
		<div class="my-10 flex flex-grow flex-col overflow-y-auto">

			<h3 class="mb-4 font-heading text-xl">Menu</h3>
			<nav class="grid gap-4">
				@include('layouts.sidebar.parts.menu-items')
			</nav>
		</div>

		{{-- Logout button --}}
		<form class="mt-auto" method="POST" action="{{ route('logout') }}">
			@csrf

			<x-link type :href="route('logout')" component="button">
				{{ __('Log Out') }}
			</x-link>
		</form>
	</div>
</header>
