<div class="flex items-center gap-3">
	<svg class="{{ request()->routeIs('dashboard') ? 'text-primary-200' : '' }} h-6 w-6" viewBox="0 0 24 24" fill="none"
		stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
		<path
			d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z" />
		<polyline points="7.5 4.21 12 6.81 16.5 4.21" />
		<polyline points="7.5 19.79 7.5 14.6 3 12" />
		<polyline points="21 12 16.5 14.6 16.5 19.79" />
		<polyline points="3.27 6.96 12 12.01 20.73 6.96" />
		<line x1="12" y1="22.08" x2="12" y2="12" />
	</svg>

	<x-link :href="route('dashboard')" :active="request()->routeIs('dashboard')">
		{{ __('Dashboard') }}
	</x-link>
</div>


<div class="flex items-center gap-3">
	<svg class="{{ request()->routeIs('') ? 'text-primary-200' : '' }} h-6 w-6" viewBox="0 0 24 24" fill="none"
		stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
		<path
			d="M22.54 6.42a2.78 2.78 0 0 0-1.94-2C18.88 4 12 4 12 4s-6.88 0-8.6.46a2.78 2.78 0 0 0-1.94 2A29 29 0 0 0 1 11.75a29 29 0 0 0 .46 5.33A2.78 2.78 0 0 0 3.4 19c1.72.46 8.6.46 8.6.46s6.88 0 8.6-.46a2.78 2.78 0 0 0 1.94-2 29 29 0 0 0 .46-5.25 29 29 0 0 0-.46-5.33z" />
		<polygon points="9.75 15.02 15.5 11.75 9.75 8.48 9.75 15.02" />
	</svg>
	<x-link :href="route('video')" :active="request()->routeIs('video')">
		{{ __('Academy') }}
	</x-link>
</div>


<div class="flex items-center gap-3">
	<svg class="{{ request()->routeIs('profile.index') ? 'text-primary-200' : '' }} h-6 w-6" viewBox="0 0 24 24"
		fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
		<path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2" />
		<circle cx="12" cy="7" r="4" />
	</svg>
	<x-link :href="route('profile.index')" :active="request()->routeIs('profile.index')">
		{{ __('Profile') }}
	</x-link>
</div>


<div class="flex items-center gap-3">
	<svg class="{{ request()->routeIs('') ? 'text-primary-200' : '' }} h-6 w-6" viewBox="0 0 24 24" fill="none"
		stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
		<circle cx="12" cy="12" r="10" />
		<line x1="2" y1="12" x2="22" y2="12" />
		<path d="M12 2a15.3 15.3 0 0 1 4 10 15.3 15.3 0 0 1-4 10 15.3 15.3 0 0 1-4-10 15.3 15.3 0 0 1 4-10z" />
	</svg>
	<x-link href="#">
		{{ __('Client Area') }}
	</x-link>
</div>


<div class="flex items-center gap-3">
	<svg class="{{ request()->routeIs('') ? 'text-primary-200' : '' }} h-6 w-6" viewBox="0 0 24 24" fill="none"
		stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
		<path d="M21 15v4a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2v-4" />
		<polyline points="7 10 12 15 17 10" />
		<line x1="12" y1="15" x2="12" y2="3" />
	</svg>
	<x-link href="#">
		{{ __('Download') }}
	</x-link>
</div>
