<div class="sticky top-[60px] hidden h-[calc(100dvh-60px)] bg-dark px-4 py-12 sm:block lg:px-8">
	<nav class="grid gap-4">
		@include('layouts.sidebar.parts.menu-items')
	</nav>
</div>
