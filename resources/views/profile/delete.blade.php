<x-profile-template>
	@section('profile-title')
		{{ __('Delete profile') }}
	@endsection

	<x-card class="h-full px-6 pb-12 pt-6">
		<p class="mb-4 text-white">
			{{ __('Once your account is deleted, all of its resources and data will be permanently deleted. Before deleting your account, please download any data or information that you wish to retain.') }}
		</p>

		<x-button class="mr-auto block" type="button" x-data=""
			x-on:click.prevent="$dispatch('open-modal', 'confirm-user-deletion')">{{ __('Delete Account') }}</x-button>

		<x-modal name="confirm-user-deletion" maxWidth="xl" :show="$errors->userDeletion->isNotEmpty()" focusable>
			<form class="p-6" method="post" action="{{ route('profile.destroy') }}">
				@csrf
				@method('delete')

				<h2 class="text-center font-heading text-lg">
					{{ __('Are you sure you want to delete your account?') }}
				</h2>

				{{-- Password --}}
				<div class="mt-4">
					<x-text-input class="mx-auto block w-full max-w-xs" name="password" type="password" required :label="__('Password')" />
					<x-input-error :messages="$errors->userDeletion->get('password')" />
				</div>

				<div class="mt-10 flex flex-wrap justify-center gap-4">
					<x-button class="min-w-[175px]" type="submit">
						{{ __('Delete Account') }}
					</x-button>

					<x-button class="min-w-[175px]" type="button" x-on:click="$dispatch('close')">
						{{ __('Cancel') }}
					</x-button>
				</div>
			</form>
		</x-modal>
	</x-card>
</x-profile-template>
