<x-profile-template>
	<x-card class="h-full px-6 pb-12 pt-6">
		<form class="flex flex-col gap-4" method="post" action="{{ route('profile.update') }}" enctype="multipart/form-data">
			@csrf
			@method('patch')


			<div class="flex flex-wrap gap-4">
				{{-- First name --}}
				<div class="flex-grow basis-[max(200px,_calc(50%-1rem))]">
					<x-text-input class="block w-full" id="name" name="name" type="text" :value="old('name', $user->name)" required
						:label="__('Name')" />
					<x-input-error :messages="$errors->get('First name')" />
				</div>

				{{-- Last name --}}
				<div class="flex-grow basis-[max(200px,_calc(50%-1rem))]">
					<x-text-input class="block w-full" id="last_name" name="last_name" type="text" :value="old('last_name', $user->last_name)" required
						:label="__('Last name')" />
					<x-input-error :messages="$errors->get('last_name')" />
				</div>

				{{-- Email --}}
				<div class="flex-grow basis-[max(200px,_calc(50%-1rem))]">
					<x-text-input class="block w-full" id="email" name="email" type="email" :value="old('email', $user->email)" required readonly
						:label="__('Email')" />
					<x-input-error :messages="$errors->get('email')" />
				</div>

				{{-- Phone --}}
				{{-- <div class="flex-grow basis-[max(200px,_calc(50%-1rem))]">
						<x-text-input class="block w-full" id="phone" name="phone" type="tel" :value="old('phone')" required
							:label="__('Phone')" />
						<x-input-error :messages="$errors->get('phone')" />
					</div> --}}
			</div>


			@if ($user instanceof \Illuminate\Contracts\Auth\MustVerifyEmail && !$user->hasVerifiedEmail())
				<div>
					<p class="mt-2 text-sm text-gray-800 dark:text-gray-200">
						{{ __('Your email address is unverified.') }}

						<button
							class="rounded-md text-sm text-gray-600 underline hover:text-gray-900 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 dark:text-gray-400 dark:hover:text-gray-100 dark:focus:ring-offset-gray-800"
							form="send-verification">
							{{ __('Click here to re-send the verification email.') }}
						</button>
					</p>

					@if (session('status') === 'verification-link-sent')
						<p class="mt-2 text-sm font-medium text-green-600 dark:text-green-400">
							{{ __('A new verification link has been sent to your email address.') }}
						</p>
					@endif
				</div>
			@endif

			<x-button class="mr-auto mt-4 w-full max-w-[200px]">{{ __('Save') }}</x-button>
		</form>
	</x-card>

	<x-card class="px-6 pb-12 pt-6">
		<h3 class="mb-4 font-heading text-lg">Language</h3>
		<form class="flex flex-wrap gap-4" id="localeForm" method="post" action="{{ route('profile.update') }}"
			enctype="multipart/form-data" x-data="{
    currentLocale: '{{ app()->getLocale() }}',
    changeLocale(event) {
        this.currentLocale = event.detail
        requestAnimationFrame(() => {
            const form = document.getElementById('localeForm')
            form.submit()
        })
    }
}">
			@csrf
			@method('patch')
			<input name="locale" type="hidden" x-bind:value="currentLocale">
			<div class="flex-grow basis-[max(200px,_calc(50%-1rem))]">
				<x-language-switcher x-on:change-locale="changeLocale" />
			</div>
		</form>
	</x-card>
</x-profile-template>
