@if (session('status') === 'password-updated')
	<script>
		document.addEventListener('DOMContentLoaded', () => {
			showToast({
				title: 'Saved',
				type: 'success'
			});
		});
	</script>
@endif

<x-profile-template>
	@section('profile-title')
		{{ __('Update password') }}
	@endsection

	<x-card class="h-full px-6 pb-12 pt-6">
		<p class="text-sm text-white">
			{{ __('Ensure your account is using a long, random password to stay secure.') }}
		</p>

		<form class="mt-4 flex flex-col gap-4" method="post" action="{{ route('password.update') }}">
			@csrf
			@method('put')

			<div class="flex flex-wrap gap-4">
				<div class="w-full flex-grow">
					<x-text-input class="block w-full" id="current_password" name="current_password" type="password"
						autocomplete="current-password" :label="__('Current Password')" />
					<x-input-error :messages="$errors->updatePassword->get('current_password')" />
				</div>

				<div class="w-full flex-grow">
					<x-text-input class="block w-full" id="password" name="password" type="password" autocomplete="new-password"
						:label="__('New Password')" />
					<x-input-error :messages="$errors->updatePassword->get('password')" />
				</div>

				<div class="w-full flex-grow">
					<x-text-input class="block w-full" id="password_confirmation" name="password_confirmation" type="password"
						autocomplete="new-password" :label="__('Confirm Password')" />
					<x-input-error :messages="$errors->updatePassword->get('password_confirmation')" />
				</div>
			</div>

			<x-button class="mr-auto mt-4 w-full max-w-[200px]">{{ __('Save') }}</x-button>
		</form>
	</x-card>
</x-profile-template>
