<div class="flex flex-row flex-wrap items-center gap-4 px-4">
	<x-link :href="route('profile.index')" :active="request()->routeIs('profile.index')">Edit profile</x-link>

	<x-link :href="route('profile.update-password')" :active="request()->routeIs('profile.update-password')">Change password</x-link>

	<x-link :href="route('profile.delete')" :active="request()->routeIs('profile.delete')">Delete account</x-link>
</div>
