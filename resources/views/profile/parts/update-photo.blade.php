<script>
	function profilePhotoState() {
		return {
			updatePhoto: (event) => {
				if (event.target.files.length > 0) {
					const src = URL.createObjectURL(event.target.files[0]);
					document.getElementById('poto-form').submit();
				}
			},
			handleCrop() {
				const result = document.getElementById("cropResult");
				const input = document.getElementById('croppedAvatar')
				const form = document.getElementById("cropped-photo-form")

				if (window.cropper) {
					const croppedCanvas = window.cropper.getCroppedCanvas({
						width: result.offsetWidth,
						height: result.offsetHeight,
					});
					croppedCanvas.toBlob((blob) => {
						const file = new File([blob], 'cropped_image.jpg')
						const container = new DataTransfer();
						container.items.add(file);
						input.files = container.files;
						form.submit()
					})
				}
			},
			openModal() {
				this.$dispatch('open-modal', 'crop');
				window.initializeCropper?.()
			},
			closeModal() {
				this.$dispatch('close', 'crop');
			}
		}
	}
</script>

<x-card class="basis-2/6 px-6 py-12">
	<div class="flex flex-col gap-4" x-data="profilePhotoState()">
		<form class="m-0 grid justify-center" id="poto-form" method="post" action="{{ route('profile.update') }}"
			enctype="multipart/form-data">
			@csrf
			@method('patch')

			<input class="hidden" id="avatar" name="avatar" type="file" x-on:change="updatePhoto">

			<div
				class="mx-auto flex h-36 w-36 items-center justify-center overflow-hidden rounded-full border-[2px] border-primary-200">
				@if (Auth::user()->avatar)
					<img class="h-full w-full object-cover" id="preview" src="{{ asset(Auth::user()->avatar) }}" alt="avatar">
				@else
					<svg class="h-20 w-20 text-primary-200" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="0.5"
						stroke-linecap="round" stroke-linejoin="round">
						<path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z" />
						<circle cx="12" cy="13" r="4" />
					</svg>
				@endif
			</div>

			<p class="mt-2 text-center">
				{{ ucfirst(Auth::user()->name) }}
				{{ ucfirst(Auth::user()->last_name) }}
			</p>
		</form>

		<div class="mx-auto mt-4 flex flex-wrap items-center justify-center gap-3">
			@if (Auth::user()->avatar)
				<x-button class="flex-grow" type="button" x-on:click="document.getElementById('avatar').click()" small>
					{{ __('Edit') }}
				</x-button>
				<x-button class="flex-grow" type="button" x-on:click="openModal" small>
					{{ __('Crop') }}
				</x-button>
				<form class="m-0 flex-grow" method="post" action="{{ route('profile.avatar.destroy') }}">
					@csrf
					@method('delete')
					<x-button class="w-full" small>
						{{ __('Delete') }}
					</x-button>
				</form>
			@else
				<x-button type="button" x-on:click="document.getElementById('avatar').click()">
					<div class="flex items-center">
						<svg class="mr-2 h-6 w-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
							stroke="currentColor" stroke-width="2">
							<path stroke-linecap="round" stroke-linejoin="round"
								d="M7 16a4 4 0 01-.88-7.903A5 5 0 1115.9 6L16 6a5 5 0 011 9.9M15 13l-3-3m0 0l-3 3m3-3v12" />
						</svg>
						{{ __('Upload photo') }}
					</div>
				</x-button>
			@endif
		</div>

		@include('profile.parts.crop')

		<form class="hidden" id="cropForm" method="post" action="{{ route('profile.update') }}"
			enctype="multipart/form-data">
			@csrf
			@method('patch')
			<input class="hidden" id="avatar" name="avatar" type="file" x-on:change="updatePhoto">
		</form>
	</div>
</x-card>
