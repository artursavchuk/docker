<x-modal name="crop" extraClasses="mt-[5vh]" :show="false">
	<div class="px-8 pb-12 pt-8">
		<h3 class="mb-4 font-heading text-2xl">{{ __('Crop your image') }}</h3>

		<form id="cropped-photo-form" method="post" action="{{ route('profile.update') }}" enctype="multipart/form-data">
			@csrf
			@method('patch')
			<div class="flex flex-col gap-8 min-[500px]:flex-row">
				<div class="w-full max-w-full">
					<img class="w-full" id="cropSourceImage" src="{{ asset(Auth::user()->avatar) }}" alt="avatar">
				</div>

				<div class="flex flex-col gap-4 p-2">
					<div
						class="mx-auto flex h-36 w-36 items-center justify-center overflow-hidden rounded-full border-[2px] border-primary-200"
						id="cropResult">
					</div>
					<p class="text-center">
						{{ ucfirst(Auth::user()->name) }}
						{{ ucfirst(Auth::user()->last_name) }}
					</p>
					<input class="hidden" id="croppedAvatar" name="avatar" type="file">

					<x-button class="mt-auto" type="button" x-on:click="handleCrop">
						{{ __('Send') }}
					</x-button>
				</div>
			</div>
		</form>
	</div>
</x-modal>
