@if (session('status') === 'profile-updated')
	<script>
		document.addEventListener('DOMContentLoaded', () => {
			showToast({
				title: 'Saved',
				type: 'success'
			});
		});
	</script>
@endif

<x-app-layout>
	<x-container>

		<x-topbar class="mb-8">
			<x-slot:title>
				@section('profile-title')
					{{ __('Profile') }}
				@show
			</x-slot:title>
		</x-topbar>

		<form id="send-verification" method="post" action="{{ route('verification.send') }}">
			@csrf
		</form>

		<div class="flex flex-col gap-8 md:flex-row">
			@include('profile.parts.update-photo')

			<div class="flex w-full basis-4/6 flex-col gap-5">
				@include('profile.parts.tabs')
				{{ $slot }}
			</div>
		</div>

	</x-container>
</x-app-layout>
