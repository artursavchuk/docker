@props(['label'])


<label {{ $attributes->twMerge('flex items-center cursor-pointer relative') }}>
	<input id="terms" name="terms" class="absolute h-6 w-6 cursor-pointer opacity-0" type="checkbox" />
	<div
		class="mr-2 flex h-4 w-4 flex-shrink-0 items-center justify-center rounded border border-gray-300 bg-transparent focus-within:border-gray-300">
		<svg class="pointer-events-none hidden h-2 w-2 fill-current text-gray-300" version="1.1" viewBox="0 0 17 12"
			xmlns="http://www.w3.org/2000/svg">
			<g fill="none" fill-rule="evenodd">
				<g transform="translate(-9 -11)" fill="#fff" fill-rule="nonzero">
					<path
						d="m25.576 11.414c0.56558 0.55188 0.56558 1.4439 0 1.9961l-9.404 9.176c-0.28213 0.27529-0.65247 0.41385-1.0228 0.41385-0.37034 0-0.74068-0.13855-1.0228-0.41385l-4.7019-4.588c-0.56584-0.55188-0.56584-1.4442 0-1.9961 0.56558-0.55214 1.4798-0.55214 2.0456 0l3.679 3.5899 8.3812-8.1779c0.56558-0.55214 1.4798-0.55214 2.0456 0z" />
				</g>
			</g>
		</svg>
	</div>
	@if (isset($slot) && !isset($label))
		{{ $slot }}
	@elseif (isset($label))
		<span>{{ $label }}</span>
	@endif
</label>
