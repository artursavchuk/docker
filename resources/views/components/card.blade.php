<div {{ $attributes->twMerge('rounded-xl bg-dark shadow-lg shadow-[rgba(256,256,256,0.15)]') }}>
	{{ $slot }}
</div>
