<x-card {{ $attributes->twMerge('w-full py-7') }}>
	<div
		class="flex flex-col items-start justify-between px-4 md:flex-row md:items-center md:px-4 lg:flex-row lg:items-center lg:px-6">

		<div class="flex flex-col">
			<h1 class="font-heading text-2xl text-white">
				{{ $title }}
			</h1>
		</div>

		@if (isset($actions))
			<div class="mt-7 flex w-full items-start justify-start gap-x-4 md:mt-0 md:justify-end lg:mt-0 lg:justify-end">
				{{ $actions }}
			</div>
		@endif
	</div>
</x-card>
