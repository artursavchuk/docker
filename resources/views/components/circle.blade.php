<div
	{{ $attributes->twMerge('pointer-none circle hidden w-[300px] h-[300px] rounded-full bg-primary-200 fixed top-[50%] left-[50%] translate-x-1/2 translate-y-1/2 transition-all duration-[16s] ease-linear blur-3xl -z-[1]') }}>
</div>
