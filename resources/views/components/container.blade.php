<div {{ $attributes->twMerge('mx-auto max-w-7xl w-full px-4 lg:px-8 inline-block') }}>
	{{ $slot }}
</div>
