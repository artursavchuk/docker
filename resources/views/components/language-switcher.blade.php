@props(['small' => false, 'currentLocale' => app()->getLocale()])

@php
	$sizes = $small ? 'h-8 p-1.5' : 'h-14 p-4';
@endphp

<div
	{{ $attributes->twMerge('relative z-10 min-w-[100px] cursor-pointer appearance-none font-normal text-white shadow-none') }}
	x-data="{ open: false }">

	<div
		class="{{ $sizes }} flex w-full cursor-pointer items-center gap-[4px] rounded border border-gray-300 bg-transparent text-sm font-light leading-5 text-white"
		x-on:click="open=true" x-on:click.away="open=false">

		{{ config('app.available_locale')[$currentLocale] ?? $currentLocale }}
		<img class="ml-auto transition duration-100 ease-linear" src="/images/select-arrow.svg" alt="arrow"
			:class="{ 'rotate-180': open }">
	</div>

	<ul
		class="invisible absolute left-0 top-[calc(100%+2px)] m-0 block w-full list-none overflow-hidden rounded-b-[5px] rounded-t-sm border border-light bg-dark p-0 opacity-0 transition duration-100 ease-linear"
		:class="{ 'opacity-100 visible': open, invisible: !open }">
		@foreach (config('app.available_locale') as $key => $value)
			<li
				class="{{ app()->isLocale($key) ? 'selected' : '' }} flex w-full cursor-pointer items-center gap-[5px] p-1.5 text-white [&.selected]:relative [&.selected]:text-white [&.selected]:before:absolute [&.selected]:before:inset-0 [&.selected]:before:bg-primary-100 [&.selected]:before:opacity-50 [&.selected]:before:content-[''] hover:[&:not(&.selected)]:bg-[rgba(256,256,256,0.1)]"
				data-value="{{ $key }}"x-on:click="$dispatch('change-locale', '{{ $key }}')">
				<span class="z-10">{{ $value }}</span>
			</li>
		@endforeach
	</ul>
</div>
