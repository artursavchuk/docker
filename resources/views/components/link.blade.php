@props(['component' => 'link', 'active' => false])

@php
	$classes = "relative block w-fit after:content-[''] after:block after:absolute after:translate-y-[2px] after:w-full after:h-[1px] after:bg-primary-100 after:scale-x-0 after:transition after:duration-200 after:origin-left after:hover:scale-x-100 hover:text-primary-100 transition-colors duration-200 ease-linea [&.active]:after:scale-x-100 [&.active]:text-primary-100";
	
	if ($active) {
	    $classes .= ' after:scale-x-100 text-primary-100';
	}
@endphp

@if ($component === 'link')
	<a {{ $attributes->twMerge($classes) }}>
		{{ $slot }}
	</a>
@elseif ($component === 'button')
	<button {{ $attributes->twMerge($classes) }} {{ $attributes->merge(['type' => 'submit']) }}>
		{{ $slot }}
	</button>
@endif
