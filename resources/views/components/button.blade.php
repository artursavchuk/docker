@props(['small' => false])

@php
	$sizes = $small ? 'min-h-[20px] px-4 py-1' : 'min-h-[48px] px-6 py-2';
@endphp

<button
	{{ $attributes->twMerge($sizes, 'inline-block rounded-full bg-primary-200 font-normal font-medium uppercase leading-normal text-white transition duration-200 ease-linear focus:outline-none focus:ring-0 active:bg-primary-100 hover:bg-primary-100') }}
	{{ $attributes->merge(['type' => 'submit']) }}>
	{{ $slot }}
</button>
