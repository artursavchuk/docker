@props(['disabled' => false, 'label' => ''])

<div class="{{ $attributes->merge(['class' => 'relative flex flex-col'])->get('class') }}">
	<div class="relative z-0 mt-2">
		<input
			class="reative peer block h-14 w-full overflow-x-auto rounded border border-gray-300 bg-transparent px-4 pb-2 pt-4 text-sm font-light leading-5 text-white transition focus:border-gray-300 focus:bg-[rgba(256,256,256,0.05)] focus:outline-none focus:ring-0"
			{{ $disabled ? 'disabled' : '' }} {{ $attributes->except('class') }} placeholder=" ">

		<label
			class="peer-focus:bg-surface-100 absolute left-4 top-4 z-10 origin-[0] -translate-x-[0.2rem] -translate-y-3 scale-75 transform px-1 text-sm font-normal tracking-[.03125em] text-gray-300 duration-300 peer-placeholder-shown:translate-y-1 peer-placeholder-shown:scale-100 peer-focus:left-4 peer-focus:-translate-x-[0.2rem] peer-focus:-translate-y-3 peer-focus:scale-75 peer-focus:px-1 peer-focus:text-gray-300"
			for="{{ $attributes->get('id') }}">{{ $label }}</label>
	</div>
</div>
