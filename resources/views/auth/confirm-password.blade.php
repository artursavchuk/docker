<x-guest-layout>
	<div class="mx-auto max-w-lg">
		<h1 class="font-heading text-4xl font-normal sm:text-6xl">{{ __('Confirm password') }}</h1>

		<p class="mt-6 text-sm text-white">
			{{ __('This is a secure area of the application. Please confirm your password before continuing.') }}
		</p>


		<form class="mt-10 flex flex-col gap-6" method="POST" action="{{ route('password.confirm') }}">
			@csrf

			{{-- Password --}}
			<div>
				<x-text-input class="block w-full" id="password" name="password" type="password" required
					autocomplete="current-password" :label="__('Password')" />
				<x-input-error :messages="$errors->get('password')" />
			</div>

			<x-button class="mx-auto mt-4 block w-full max-w-xs">{{ __('Confirm') }}</x-button>
		</form>
	</div>
</x-guest-layout>
