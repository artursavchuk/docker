<x-guest-layout>
	<div class="mx-auto max-w-lg">
		<h1 class="font-heading text-4xl font-normal sm:text-6xl">{{ __('Forgot password') }}</h1>

		<p class="mt-6 text-sm text-white">
			{{ __('Forgot your password? No problem. Just let us know your email address and we will email you a password reset link that will allow you to choose a new one.') }}
		</p>

		{{-- <x-auth-session-status class="mb-4" :status="session('status')" /> --}}

		<form class="mt-10 flex flex-col gap-6" method="POST" action="{{ route('password.email') }}">
			@csrf

			{{-- Email Address --}}
			<div>
				<x-text-input class="block w-full" id="email" name="email" type="email" :value="old('email')" required autofocus
					:label="__('Email')" />
				<x-input-error :messages="$errors->get('email')" />
			</div>

			<x-button class="mx-auto mt-4 block w-full max-w-xs">{{ __('Email Password Reset Link') }}</x-button>

			{{-- Login --}}
			<x-link class="mx-auto" href="{{ route('login', ['locale' => App::currentLocale()]) }}">{{ __('Login') }}</x-link>
		</form>
	</div>
</x-guest-layout>
