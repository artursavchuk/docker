<x-guest-layout>
	<div class="mx-auto max-w-lg" x-data>
		<h1 class="font-heading text-4xl font-normal sm:text-6xl">{{ __('Login') }}</h1>
		<form class="mt-10 flex flex-col gap-6" method="POST" action="{{ route('login') }}">
			@csrf

			{{-- Email --}}
			<div> <x-text-input class="block w-full" id="email" name="email" type="email" :value="old('email')" required
					autocomplete="username" :label="__('Email')" />
				<x-input-error :messages="$errors->get('email')" />
			</div>

			{{-- Password --}}
			<div>
				<x-text-input class="block w-full" id="password" name="password" type="password" required
					autocomplete="current-password" :label="__('Password')" />
				<x-input-error :messages="$errors->get('password')" />
			</div>

			<div class="mt-4 flex-wrap items-start justify-between gap-4 max-[500px]:grid min-[500px]:flex sm:mt-0">
				{{-- Remember Me --}}
				<x-checkbox id="remember_me" name="remember" :label="__('Remember me')" />

				<div class="grid gap-3 max-[500px]:mt-4">
					{{--  Forgot password --}}
					@if (Route::has('password.request'))
						<x-link class="min-[500px]:justify-self-end"
							href="{{ route('password.request', ['locale' => App::currentLocale()]) }}">{{ __('Forgot your password?') }}</x-link>
					@endif

					{{-- Registration --}}
					<x-link class="min-[500px]:justify-self-end"
						href="{{ route('register', ['locale' => App::currentLocale()]) }}">{{ __('Registration') }}</x-link>
				</div>
			</div>

			<x-button class="mx-auto mt-3 block w-full max-w-xs">{{ __('Log in') }}</x-button>

			{{-- social auth --}}
			<div class="mt-4">
				<p class="mb-3 text-center">{{ __('Or sign in with') }}</p>
				<div class="flex flex-wrap items-center justify-center gap-4">
					<button class="flex h-12 w-12 items-center justify-center rounded-full bg-white" type="button"
						x-on:click="alert('google')">
						<img class="h-6 w-6" src="/images/google.svg" alt="google logo">
					</button>

					<button class="flex h-12 w-12 items-center justify-center rounded-full bg-white" type="button"
						x-on:click="alert('apple')">
						<img class="h-6 w-6" src="/images/apple.svg" alt="apple logo">
					</button>

					<button class="flex h-12 w-12 items-center justify-center rounded-full bg-[#1447A0]" type="button"
						x-on:click="alert('facebook')">
						<img class="h-6 w-6" src="/images/facebook.svg" alt="facebook logo">
					</button>
				</div>

			</div>

		</form>
	</div>
</x-guest-layout>
