<x-guest-layout>
	<div class="mx-auto max-w-lg">
		<h1 class="font-heading text-4xl font-normal sm:text-6xl">{{ __('Registration') }}</h1>

		<form class="mt-10 flex flex-col gap-6" method="POST" action="{{ route('register') }}">
			@csrf

			{{-- First name --}}
			<div>
				<x-text-input class="block w-full" id="name" name="name" type="text" :value="old('name')" required
					:label="__('First name')" />
				<x-input-error :messages="$errors->get('name')" />
			</div>

			{{-- Last name --}}
			<div>
				<x-text-input class="block w-full" id="last_name" name="last_name" type="text" :value="old('last_name')" required
					:label="__('Last name')" />
				<x-input-error :messages="$errors->get('last_name')" />
			</div>

			{{-- Email --}}
			<div>
				<x-text-input class="block w-full" id="email" name="email" type="email" :value="old('email')" required
					:label="__('Email')" />
				<x-input-error :messages="$errors->get('email')" />
			</div>

			{{-- Phone --}}
			{{-- <div>
				<x-text-input class="block w-full" id="phone" name="phone" type="tel" :value="old('phone')" required
					:label="__('Phone')" />
				<x-input-error :messages="$errors->get('phone')" />
			</div> --}}

			{{-- Password --}}
			<div>
				<x-text-input class="block w-full" id="password" name="password" type="password" required :label="__('Password')" />
				<x-input-error :messages="$errors->get('password')" />
			</div>

			{{-- Confirm Password --}}
			<div>
				<x-text-input class="block w-full" id="password_confirmation" name="password_confirmation" type="password" required
					:label="__('Confirm Password')" />
				<x-input-error :messages="$errors->get('password_confirmation')" />
			</div>

			<div class="mt-4 flex-wrap items-start justify-between gap-4 max-[500px]:grid min-[500px]:flex sm:mt-0">
				{{-- Agree --}}
				<div>
					<x-checkbox id="terms" name="terms">
						<span>
							{{ __('Agree with') }}
							<x-link class="inline-block"
								href="{{ route('login', ['locale' => App::currentLocale()]) }}">{{ __('terms and conditions') }}</x-link>
						</span>
					</x-checkbox>
					<x-input-error :messages="$errors->get('terms')" />
				</div>

				{{-- Login --}}
				<x-link href="{{ route('login', ['locale' => App::currentLocale()]) }}">{{ __('Login') }}</x-link>
			</div>

			<x-button class="mx-auto mt-4 block w-full max-w-xs">{{ __('Register') }}</x-button>
		</form>
	</div>

</x-guest-layout>
