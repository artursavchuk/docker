<x-guest-layout>
	<div class="mx-auto max-w-lg">
		<h1 class="font-heading text-4xl font-normal sm:text-6xl">{{ __('Reset password') }}</h1>

		<form class="mt-10 flex flex-col gap-6" method="POST" action="{{ route('password.store') }}">
			@csrf

			{{-- Password Reset Token --}}
			<input name="token" type="hidden" value="{{ $request->route('token') }}">

			{{-- Email --}}
			<div>
				<x-text-input class="block w-full" id="email" name="email" type="email" :value="old('email', $request->email)" required autofocus
					autocomplete="username" :label="__('Email')" />
				<x-input-error :messages="$errors->get('email')" />
			</div>

			<!-- Password -->
			<div>
				<x-text-input class="block w-full" id="password" name="password" type="password" required
					autocomplete="new-password" :label="__('Password')" />
				<x-input-error :messages="$errors->get('password')" />
			</div>

			<!-- Confirm Password -->
			<div>
				<x-text-input class="block w-full" id="password_confirmation" name="password_confirmation" type="password" required
					autocomplete="new-password" :label="__('Confirm Password')" />
				<x-input-error :messages="$errors->get('password_confirmation')" />
			</div>

			<x-button class="mx-auto mt-4 block w-full max-w-xs">{{ __('Reset Password') }}</x-button>
		</form>
	</div>
</x-guest-layout>
