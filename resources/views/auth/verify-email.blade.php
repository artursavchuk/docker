<x-guest-layout>
	<div class="mx-auto max-w-lg">
		<h1 class="font-heading text-4xl font-normal sm:text-6xl">{{ __('Email Verification') }}</h1>

		<p class="mt-6 text-sm text-white">
			{{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
		</p>

		@if (session('status') == 'verification-link-sent')
			<div class="text-green-60 mb-4 text-sm font-medium">
				{{ __('A new verification link has been sent to the email address you provided during registration.') }}
			</div>
		@endif

		<div
			class="mt-10 flex flex-col items-center justify-center gap-x-4 gap-y-8 min-[500px]:flex-row min-[500px]:justify-between">
			<form class="max-[500px]:w-full" method="POST" action="{{ route('verification.send') }}">
				@csrf
				<x-button class="max-[500px]:w-full">
					{{ __('Resend Verification Email') }}
				</x-button>
			</form>

			<form method="POST" action="{{ route('logout') }}">
				@csrf

				<x-link component='button'>{{ __('Log Out') }}</x-link>
			</form>
		</div>
</x-guest-layout>
