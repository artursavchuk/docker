import "../css/app.scss";
import "./bootstrap.js";
import "./circle.js";
import "./iziToast";
import "./crop.js";

import Alpine from "alpinejs";

window.Alpine = Alpine;

Alpine.start();
