import AOS from "aos";

window.addEventListener("DOMContentLoaded", () => {
    AOS.init();
    var circles = document.querySelectorAll(".circle");
    var circle1 = document.querySelector("#circle1");
    var circle2 = document.querySelector("#circle2");

    function moveCircle(circle, maxX, maxY, otherCircle) {
        var newX = Math.floor(Math.random() * maxX);
        var newY = Math.floor(Math.random() * maxY);

        // var distance = Math.sqrt(
        //     Math.pow(newX - otherCircle.offsetLeft, 2) +
        //         Math.pow(newY - otherCircle.offsetTop, 2)
        // );

        // if (distance < circle.offsetWidth + otherCircle.offsetWidth) {
        //     moveCircle(circle, maxX, maxY, otherCircle);
        //     return;
        // }

        circle.style.left = newX + "px";
        circle.style.top = newY + "px";
    }

    circles.forEach((circle) => {
        var maxX = window.innerWidth - circle.offsetWidth;
        var maxY = window.innerHeight - circle.offsetHeight;

        if (circle == circle1) {
            var otherCircle = circle2;
        } else {
            var otherCircle = circle1;
        }

        circle.classList.remove("hidden");

        setInterval(() => moveCircle(circle, maxX, maxY, otherCircle), 3000);
    });
});
