import "cropperjs/dist/cropper.css";
import Cropper from "cropperjs";

let cropper;

function initializeCropper() {
    const image = document.getElementById("cropSourceImage");
    const result = document.getElementById("cropResult");

    if (cropper) {
        cropper.destroy();
    }

    cropper = new Cropper(image, {
        viewMode: 3,
        aspectRatio: 1,
        crop() {
            result.innerHTML = "";
            result.appendChild(
                cropper.getCroppedCanvas({
                    width: result.offsetWidth,
                    height: result.offsetHeight,
                })
            );
        },
    });
    window.cropper = cropper;
}

window.initializeCropper = initializeCropper;
window.addEventListener("resize", initializeCropper);
