import "izitoast/dist/css/iziToast.min.css";
import iziToast from "izitoast";

const colors = {
    success: "#28a745",
    warning: "#ffc107",
    error: "#dc3545",
    info: "#007bff",
};

const showToast = ({ title = "", message = "", type = "success" }) => {
    iziToast.show({
        color: colors[type] ?? colors.info,
        titleColor: "#ffffff",
        messageColor: "#ffffff",
        iconColor: "#ffffff",
        position: "topRight",
        theme: "dark",
        baloon: false,
        zindex: 50,
        title,
        message,
    });
};

window.iziToast = iziToast;
window.showToast = showToast;
