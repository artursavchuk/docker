<?php

namespace Feature\User;

use Illuminate\Http\Testing\File;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileUpdateTest extends TestCase
{
    use RefreshDatabase;

    public function test_it_failed_if_user_unauthorized(): void
    {
        $response = $this
            ->get('/profile');

        $response->assertFound();
    }

    public function test_it_success_update_avatar(): void
    {
        $user = User::factory()->create();

        $avatar = File::image('fake.jpg');
        $response = $this
            ->actingAs($user)
            ->patch('/profile', [
                'avatar' => $avatar,
            ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertRedirect('/');

        $user->refresh();
        $this->assertNotNull($user->avatar);
    }

    public function test_it_success_remove_avatar(): void
    {
        $user = User::factory()->create();

        $avatar = File::image('fake.jpg');
        $this
            ->actingAs($user)
            ->patch(route('profile.update'), [
                'avatar' => $avatar,
            ]);
        $user->refresh();
        $this->assertNotNull($user->avatar);

        $response = $this
            ->actingAs($user)
            ->delete(route('profile.avatar.destroy'));

        $response->assertSessionHasNoErrors();
        $user->refresh();
        $this->assertNull($user->avatar);
    }

    public function test_it_success(): void
    {
        $user = User::factory()->create();

        $response = $this
            ->actingAs($user)
            ->patch('/profile', [
                'name' => 'Test User',
                'email' => 'test@example.com',
            ]);

        $response
            ->assertSessionHasNoErrors()
            ->assertRedirect('/');

        $user->refresh();

        $this->assertSame('Test User', $user->name);
        $this->assertSame('test@example.com', $user->email);
        $this->assertNull($user->email_verified_at);
    }

}
