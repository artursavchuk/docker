<?php

namespace Tests\Feature\Auth;

use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegistrationTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function test_registration_screen_can_be_rendered(): void
    {
        $response = $this->get('/register');

        $response->assertStatus(200);
    }

    public function test_it_failed_when_terms_not_accepted(): void
    {
        $response = $this->post('/register', [
            'name' => $this->faker->name,
            'last_name' => $this->faker->lastName,
            'email' => 'test@example.com',
            'password' => $passwd = 'StrngPsswf3431',
            'password_confirmation' => $passwd,
        ]);

        $response->assertInvalid();
        $response->assertRedirect();
    }

    public function test_new_users_can_register(): void
    {
        $response = $this->post('/register', [
            'name' => $this->faker->name,
            'last_name' => $this->faker->lastName,
            'email' => 'test@example.com',
            'password' => $passwd = 'StrngPsswf3431',
            'password_confirmation' => $passwd,
            'terms' => true
        ]);

        $this->assertAuthenticated();
        $response->assertRedirect(RouteServiceProvider::HOME);
    }
}
