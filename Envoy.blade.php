@setup
require __DIR__.'/vendor/autoload.php';

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);

try {
$dotenv->load();
} catch ( Exception $e )  {
echo $e->getMessage();
}

$repository = env('CI_REPOSITORY_URL');
$branch     = env('CI_COMMIT_BRANCH');

$type   = preg_match('/^release\/.*/', $branch, $matches) ? 'release' : $branch;

$directory  = env('DEPLOY_BASE_DIR_' . $type) ?? env('DEPLOY_BASE_DIR');

$releaseDir = sprintf("%s/releases", $directory);
$currentDir = sprintf("%s/current", $directory);

$release = date('YmdHis');

$currentReleaseDir = sprintf("%s/%s", $releaseDir, $release);

$user   = env('DEPLOY_USER_' . $type) ?? env('DEPLOY_USER');
$host   = env('DEPLOY_SERVER_' . $type) ?? env('DEPLOY_SERVER');

function log_message($message) {
return "echo '\033[32m" .$message. "\033[0m';\n";
}
@endsetup

@servers(['server' => sprintf("%s@%s", $user, $host)])

@story('deploy', ['on' => 'server'])
git
prepare_storage
composer
update_symlinks
migrate
clean_old_releases
@endstory

@task('prepare_storage')
{{ log_message("Creating storage directories if not exist") }}
cd {{$directory}}
mkdir -p {{$directory}}/storage/app/public
mkdir -p {{$directory}}/storage/framework/cache/data
mkdir -p {{$directory}}/storage/framework/sessions
mkdir -p {{$directory}}/storage/framework/testing
mkdir -p {{$directory}}/storage/framework/views
mkdir -p {{$directory}}/storage/logs
@endtask

@task('git')
{{ log_message("Cloning repository") }}

git clone {{ $repository }} --branch={{ $branch }} --depth=1 -q {{ $currentReleaseDir }}
rm -rf {{ $currentReleaseDir }}/.git
@endtask

@task('composer')
{{ log_message("Running composer") }}

cd {{ $currentReleaseDir }}
ln -nfs {{ $directory }}/.env .env;

/opt/php81/bin/php ../../composer.phar install --no-interaction --quiet --prefer-dist --optimize-autoloader {{ $branch == 'master' ? '--no-dev' : '' }}
npm i
npm run build
@endtask

@task('update_symlinks')
{{ log_message("Updating symlinks") }}

# Remove the storage directory and replace with persistent data
{{ log_message("Linking storage directory") }}
rm -rf {{ $currentReleaseDir }}/storage;
cd {{ $currentReleaseDir }};
ln -nfs {{ $directory }}/storage {{ $currentReleaseDir }}/storage;
ln -nfs {{ $directory }}/storage/app/public {{ $currentReleaseDir }}/public/storage

# Import the environment config
{{ log_message("Linking .env file") }}
cd {{ $currentReleaseDir }};
ln -nfs {{ $directory }}/.env .env;

# Symlink the latest release to the current directory
{{ log_message("Linking current release") }}
ln -nfs {{ $currentReleaseDir }} {{ $currentDir }};
@endtask

@task('set_permissions')
# Set dir permissions
{{ log_message("Set permissions") }}

chmod -R ug+rwx {{ $directory }}/storage
cd {{ $directory }}
@endtask

@task('migrate')
{{ log_message("Running migrations") }}

/opt/php81/bin/php {{ $currentDir }}/artisan migrate --seed --force
@endtask

@task('clean_old_releases')
# Delete all but the 5 most recent releases
{{ log_message("Cleaning old releases") }}
cd {{ $releaseDir }}
ls -dt {{ $releaseDir }}/* | tail -n +3 | xargs -d "\n" rm -rf;
@endtask

