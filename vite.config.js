import laravel from "laravel-vite-plugin";
import { defineConfig } from "vite";

var config = {
    plugins: [
        laravel({
            input: ["resources/css/app.scss", "resources/js/app.js"],
        }),
    ],
    build: {
        assetsDir: "",
    },
    server: {
        host: true,
        hmr: {
            host: "localhost",
        },
    },
};

export default defineConfig(({ command }) => {
    if (command === "serve") {
        config.publicDir = "public";
    }

    return config;
});
