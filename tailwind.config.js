import defaultTheme from "tailwindcss/defaultTheme";
import forms from "@tailwindcss/forms";

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        "./vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php",
        "./storage/framework/views/*.php",
        "./resources/views/**/*.blade.php",
    ],

    darkMode: "media", // or 'media' or 'class'

    theme: {
        extend: {
            fontFamily: {
                heading: ["Audiowide", ...defaultTheme.fontFamily.sans],
                sans: ["Poppins", ...defaultTheme.fontFamily.sans],
                serif: ["Poppins", ...defaultTheme.fontFamily.sans],
            },

            colors: {
                light: "#DCDCDC",
                dark: "#28282f",
                primary: {
                    100: "#EE8065",
                    200: "#EA603F",
                },
            },

            backgroundImage: {
                "texture-1": "url(/images/texture-1.png)",
                "texture-2": "url(/images/texture-2.jpg)",
            },
        },
    },

    plugins: [forms],
};
