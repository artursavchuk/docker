<?php

declare(strict_types=1);

namespace App\Http\Utils;

final class FilesUtils
{
    public const IMAGE_MIMETYPES = [
        'image/jpeg',
        'image/png',
        'image/webp',
    ];

    public const DOC_MIMETYPES = [
        'application/pdf',
    ];

    public const ZIP_MIMETYPES = [
        'application/zip',
        'application/octet-stream',
        'application/x-zip-compressed',
        'multipart/x-zip',
    ];

    public const DOC_SCAN_MIMETYPES = [
        ...self::IMAGE_MIMETYPES,
        ...self::DOC_MIMETYPES,
    ];
}
