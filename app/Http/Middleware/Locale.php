<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if($request->user()) {
            $locale = $request->user()->locale ?? config('app.fallback_locale');
        } else {
            $query = request()->query('locale');
            $locale = in_array($query ,array_keys(config('app.available_locale')))
                ? $query
                : config('app.fallback_locale');
        }

        app()->setLocale($locale);

        return $next($request);
    }
}
