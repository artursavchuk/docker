<?php

namespace App\Http\Requests;

use App\Http\Utils\FilesUtils;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\Rule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['string', 'max:255'],
            'last_name' => ['string', 'max:255'],
            'email' => ['email', 'max:255', Rule::unique(User::class)->ignore($this->user()->id)],
            'locale' => ['string', Rule::in(array_keys(config('app.available_locale')))],
            'avatar' => [
                'file',
                sprintf('mimetypes:%s', implode(',', FilesUtils::IMAGE_MIMETYPES)),
                sprintf('max:%s', config('media-library.max_file_size')),
            ],
        ];
    }
}
