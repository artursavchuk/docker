# backend
create-backend:
	@make up-backend
	@make composer-install
	@make artisan-migrate
	@make artisan-storage-link
	
up-backend:
	docker-compose up -d --build nginx

composer-install:
	docker-compose run --rm composer install

artisan-migrate:
	docker-compose run --rm artisan migrate

artisan-storage-link:
	docker-compose run --rm artisan storage:link

stop-backend:
	docker-compose down

# frontend
npm-install:
	docker-compose run --rm --name npm-install npm install

up-frontend:
	docker-compose run --rm --name vite -p 5173:5173 npm run dev

stop-frontend:
	docker stop vite 
